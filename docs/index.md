# The LKCamp documentation

This is the LKCamp documentation.
First version written by Helen Koike Fornazier and Gabriel Krisman Bertazi.
Second version written by Gabriela Bittencourt and Vinícius Peixoto.

Copyright (c) 2018 Helen Fornazier <helen.fornazier@gmail.com>

Copyright (c) 2018 Gabriel Krisman Bertazi <gabriel@krisman.be>

Copyright (c) 2024 Gabriela Bittencourt <gbittencourt@lkcamp.dev>

Copyright (c) 2024 Vinícius Peixoto <vpeixoto@lkcamp.dev>

In continuous improvement by members of LKCamp.

This document is licensed, including subpages under the Creative Commons
Attribution-ShareAlike 4.0 International license.  You should have
received a copy of the license along with this work.  If not, see
<http://creativecommons.org/licenses/by-sa/4.0/>.

