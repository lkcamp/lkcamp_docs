# Reaching out to the community

As a decentralized project, Linux has many communication channels.
It's important in any project to know how and where to ask for help.

!!! warning
	Before asking questions remember that many of these people are volunteers.
	Being polite and showing that you did some research on the topic beforehand will help you with getting the attention from experts,
	for example: "I found the documentation X, but I couldn't understand Y, could someone help me by explaining what the command Z means please?"

The primary forms of communication are mailing lists and IRC channels.

??? note "Why Mailing lists?"
	Linux development is completely centered around Mailing lists, and this is completely unlikely to change any time soon.
	Hence, it is extremely important that each and everyone of us become fully capable of interacting and asynchronously discussing topics via email.
	
	The quickest way to get screamed at in a kernel related mailing list is to ignore general mailing list etiquette.
	Email etiquette is all about being considerate of your reader so, before ever submitting a message to a mailing list, be it lkcamp, kml or any list, make sure you understand the rules of that community.
	??? note "Common mistakes with dealing with Mailing lists"
		* Doing Top-posting instead of Bottom-posting: In Linux kernel discussions we use interleaved (or "inline") replies in order to make conversations easier to follow, see: [wikipedia.org/wiki/Posting_style#Interleaved_style](https://en.wikipedia.org/wiki/Posting_style#Interleaved_style)
		* Sending HTML-formatted messages: most mailing lists block emails formatted with HTML. Kernel developers need to be able to read and comment on your mail or the changes you are submitting. So all mails have to be written in **plain text** (No MIME, no links, no compression, no attachments).
	
	!!! Danger "Beware"
		If you use Gmail, remember to at least use "Plain text mode" when replying.
		Mailing lists usually block formatted emails.
		And remember to do bottom-posting.

??? note "IRC (Internet Relay Chat)"
	IRC is a forum-like chat system so exchanging messages can be faster than in Mailing lists.
	You can find more information on [kernelnewbies.org/IRC](https://kernelnewbies.org/IRC).
	
	To use it, install any IRC client, we recommend `hexchat` for beginners.

Here are some useful pointer to be in contact with:

## 1. LKCamp

Use our official communication channels, click to see instructions:

??? note "Telegram community"
	[t.me/lkcamp](https://t.me/lkcamp) is our official Telegram community, mainly in Portuguese.
	
	Use it to post quick questions and share progress. :)

??? note "Mailing list for discussions"
	Internal list for communication of this community, mainly in Portuguese.
	
	Use this Mailing list for questions that don’t fit the kernelnewbies guidelines,
	for questions specific to this group or for discussions about this document.
	
	Also, when submitting questions to kernelnewbies,
	add this mailing list in CC to request feedback from us.
	
	Subscribe to this list through [this link](https://lists.sr.ht/~lkcamp/discussion/)

??? note "Mailing list for sending patches"
	Mailing list dedicated to send and review patches from this group.
	
	Use this list to view the work made by members of the group.
	
	Subscribe to this list through [this link](https://lists.sr.ht/~lkcamp/patches)

## 2. Kernel newbies

This project is aimed to help new Linux kernel developers.
Take a look at [their website](https://kernelnewbies.org).

??? note "Mailing list kernelnewbies"
	Use this list to post questions about kernel development.
	
	Make sure you read the kernelnewbies [mailing list guidelines](https://kernelnewbies.org/mailinglistguidelines)
	before submitting any message.
	
	Subscribe to the list through [this link](http://lists.kernelnewbies.org/mailman/listinfo/kernelnewbies).

??? note "IRC channel"
	Use this channel for questions regarding kernel development (basically the same rules from mailing
	list guideline apply here).
	
	Join the channel: "**#kernelnewbies @ irc.oftc.net**"

## 3. Mentorship Program of Linux Foundation

If you are searching for a mentorship program to develop for kernel in a structured remote learning manner,
you can visit the opportunities of [LFX Mentorship](https://mentorship.lfx.linuxfoundation.org/)
and search for 'Linux Kernel'.
You can have a look at the [Beginner's Guide to Linux Kernel Development](https://wiki.linuxfoundation.org/lkmp) free e-course.

??? note "Mailing list Linux kernel mentees"
	Use this list to post questions related to the mentorship program.
	
	You can subscribe nonetheless to follow the discussion of the mentees and mentors.
	
	Subscribe to the list through [this link](https://lists.linuxfoundation.org/mailman/listinfo/linux-kernel-mentees)

??? note "IRC channel"
	Use this channel for questions regarding kernel development.
	Most people on this channel will be beginner kernel developers, so don't be afraid to ask questions.
	
	Join the channel: "**#kernel-mentee @ irc.oftc.net**"
